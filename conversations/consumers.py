from channels.generic.websocket import WebsocketConsumer
from conversations.models import Message
from asgiref.sync import async_to_sync

import json
from datetime import datetime


class Consumer(WebsocketConsumer):
    def connect(self):
        self.chat_name = self.scope['url_route']['kwargs']['room_name']
        self.chat_group_name = f'chat_{self.chat_name}'
        async_to_sync(self.channel_layer.group_add)(self.chat_group_name,
                                                    self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.chat_group_name, self.channel_name)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        new_message = Message(sending_time=datetime.now(),
                              conversation_id=text_data_json['conversation'],
                              sender_id=text_data_json['user'],
                              content=text_data_json['message'])
        new_message.save()
        async_to_sync(self.channel_layer.group_send)(
            self.chat_group_name,
            {
                'type': 'chat_message',
                'message': text_data_json['message']
            }
        )

    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))
