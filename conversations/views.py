import json

from django.views.generic import ListView, View, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from json_views.views import JSONListView
from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import UserSearchForm
from .models import User, Conversation


def index(request):
    return render(request, 'base.html')


class ConversationsView(LoginRequiredMixin, ListView):
    context_object_name = 'conversations'
    template_name = 'conversations.html'

    def get_queryset(self):
        conversations = self.request.user.conversation_set.all()
        for conversation in conversations:
            if conversation.is_private:
                conversation.interlocutor = conversation.get_interlocutor(self.request.user)
        return conversations

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ConversationsView, self).get_context_data(**kwargs)
        context['title'] = 'Conversations'
        return context


class MessagesView(LoginRequiredMixin, ListView):
    context_object_name = 'messages'
    template_name = 'messages.html'

    def get_queryset(self):
        chat = self.kwargs['chat']
        messages = self.request.user.conversation_set.get(title=chat).message_set.all()
        return messages

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        context['user'] = self.request.user
        context['title'] = self.kwargs['chat']
        conversation = Conversation.objects.get(title=context['title'])
        #TODO: get interlocutor name to show it in title
        context['chat'] = conversation
        return context


class JsonConversationMessagesView(LoginRequiredMixin, JSONListView):
    def get_queryset(self):
        chat = self.kwargs['chat']
        messages = self.request.user.conversation_set.get(id=chat).message_set.all()
        response_content = []

        for message in messages:
            response_content.append({
                'sending_time': message.sending_time,
                'conversation_id': message.conversation.id,
                'content': message.content,
                'sender': message.sender
            })
        return response_content


class JsonUsersListView(LoginRequiredMixin, JSONListView):
    def get_queryset(self):
        users = []
        print(self.kwargs)
        if self.kwargs.get('username') is None:
            users =  User.objects.all()
        else:
            username = self.kwargs['username']
            users = User.objects.filter(username__icontains=username).exclude(id=self.request.user.id)
        result_users = []
        for user in users:
            result_users.append({
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name
            })
        return result_users


class UsersSearchView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        form = UserSearchForm()
        return render(request, 'user_search.html', {'form': form})


class UserView(LoginRequiredMixin, DetailView):
    template_name = 'user_detail.html'
    model = User

    slug_field = 'username'

    def get_object(self, queryset=None):
        return super(UserView, self).get_object(queryset)


class CreatePrivateConversationView(View):
    def has_chat_with_user(self, conversations, user):
        for conversation in conversations:
            users = conversation.participants.filter(id=user.id).all()
            if len(users) > 0:
                return conversation
        return False

    def post(self, _):
        username = self.request.POST.get('username')
        user = User.objects.get(username=username)
        conversations = self.request.user.conversation_set.all()
        conversation = self.has_chat_with_user(conversations, user)
        if not conversation:
            conversation = Conversation(title=f'{self.request.user.username}_{username}')
            conversation.is_private = True
            conversation.save()
            conversation.participants.add(user, self.request.user)

        return JsonResponse({'title': conversation.title})
