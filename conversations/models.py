from django.db import models
from django.contrib.auth.models import User


class Conversation(models.Model):
    participants = models.ManyToManyField(User)
    title = models.CharField(max_length=64)
    is_private = models.BooleanField(default=False)

    def last_message(self):
        return self.message_set.order_by('-sending_time').first()

    def get_interlocutor(self, user):
        return self.participants.exclude(id=user.id).all()[0]


class Message(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    sending_time = models.DateTimeField()
    content = models.TextField()

    def __repr__(self):
        return '"{}" sent by {} at {}'.format(self.content, self.sender.username, self.sending_time)
