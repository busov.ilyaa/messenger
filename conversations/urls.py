from django.urls import path
from django.conf.urls import url
from .views import ConversationsView, \
    MessagesView, JsonConversationMessagesView, JsonUsersListView, UsersSearchView, UserView, CreatePrivateConversationView, index

urlpatterns = [
    path('', ConversationsView.as_view(), name='index'),
    url(r'api/users/(?P<username>.*)/$', JsonUsersListView.as_view(), name='users-list'),
    path('users/', UsersSearchView.as_view(), name='user-search'),
    path('api/users/', JsonUsersListView.as_view(), name='users-list'),
    path('chat/<str:chat>', MessagesView.as_view(), name='messages'),
    path('users/<str:slug>', UserView.as_view(), name='user'),
    path('api/chat/add', CreatePrivateConversationView.as_view(), name='add-chat'),
    path('api/chat/<str:chat>', JsonConversationMessagesView.as_view(), name='messages-json'),
]
